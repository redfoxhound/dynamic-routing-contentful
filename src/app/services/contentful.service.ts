import { Injectable } from '@angular/core';
import { createClient, Entry } from 'contentful';
import { environment } from 'src/environments/environment';
import { from, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContentfulService {

  /**
   * Simple object to handle all routes with the id as key
   */
  cacheContent: object = {};

  private cdaClient = createClient({
    space: environment.contentful.spaceID,
    accessToken: environment.contentful.accessToken
  });

  getContentEntries(id, query?: object) {
    this.cdaClient.getEntries({
      content_type: environment.contentful.contentType.lessons
    }).then(res => {
      console.log(res);
    });
  }

  getEntry(entryId): Observable<any> {
    /**
     * Handle whether content was already fetched. Otherwise fetch it from contentful
     */
    if (this.cacheContent && this.cacheContent[entryId]) {
      console.log('FROM MEMORY', this.cacheContent);
      return of(this.cacheContent[entryId]);
    }
    return from(this.cdaClient.getEntry(entryId, {
      include: 4
    }))
      .pipe(
        tap((res) => {
          console.log('FROM CONTENTFUL', res);
          this.cacheContent[res.sys.id] = res;
        })
      );
  }
}
