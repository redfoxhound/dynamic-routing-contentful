import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, AppRoutingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicComponent } from './pages/dynamic/dynamic.component';

@NgModule({
  declarations: [
    AppComponent,
    AppRoutingComponent,
    DynamicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DynamicComponent]
})
export class AppModule {
  constructor() {}
}
