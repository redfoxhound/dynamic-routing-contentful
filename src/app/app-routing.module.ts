import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { WildcardComponent } from './pages/wildcard/wildcard.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    /**
     * Wild card component is important to catch routes not through homepage
     */
    path: '**',
    component: WildcardComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      /**
       * This is to avoid a hacky method of reloading the same route
       */
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const AppRoutingComponent = [
  HomeComponent,
  WildcardComponent
];
