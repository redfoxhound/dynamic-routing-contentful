import { Component, OnInit } from '@angular/core';
import { ContentfulService } from './services/contentful.service';
import { environment } from 'src/environments/environment';
import { Router, Route } from '@angular/router';
import { DynamicComponent } from './pages/dynamic/dynamic.component';
import { RoutingService } from './services/routing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'dynamicRouting';
  customRoutes: Route[] = [];
  fetchedContentful = false;

  constructor(
    private contentfulService: ContentfulService,
    private routingService: RoutingService,
    private router: Router
  ) {}

  ngOnInit() {
    this.contentfulService.getEntry(environment.contentful.entry.homepage)
    .subscribe((res: any) => {
      this.parseNavigation(res);
      this.routingService.dynamicRoutes.next(this.customRoutes);
    });
  }

  /**
   * Called to build the new router
   * @param content the content from contentful service
   */
  parseNavigation(content: any[]): void {
    const routes = this.parseSubpages(content).children;

    // Reconfigure router config
    this.router.resetConfig([
      ...routes,
      ...this.router.config // Add last as to not overwrite the wildcard route
    ]);

    this.customRoutes = routes; // Set for app menu
    this.fetchedContentful = true; // Set to true as to not re-fetch
  }

  /**
   * 
   * @param page The current page to parse
   * @param routeInput The current route to be modified passed down through recursion
   */
  parseSubpages(page: any, routeInput?: Route) {
    let route: Route;

    if (routeInput) {
      route = routeInput;
    } else {
      route = {
        path: page.fields.slug,
        component: DynamicComponent,
        data: {
          name: page.fields.pageTitle,
          id: page.sys.id
        }
      };
    }

    if (page && page.fields && page.fields.hasOwnProperty('pages')) {
      // Pops off the first route which is home
      delete route.component;
      const pages = page.fields.pages;
      const childRoutes: Route[] = [];

      // Remove empty route for home
      if (page.fields.slug !== 'home') {
        childRoutes.push({
          path: '',
          component: DynamicComponent,
          data: {
            name: page.fields.pageTitle,
            id: page.sys.id
          }
        });
      }

      for (const subPage of Object.keys(pages)) {
        childRoutes.push(this.parseSubpages(pages[subPage]));
      }

      // Assign cildren property to the route
      route.children = childRoutes;
    }

    return route;
  }
}
