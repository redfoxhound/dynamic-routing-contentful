import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingService } from 'src/app/services/routing.service';
import { of } from 'rxjs';

@Component({
  selector: 'app-wildcard',
  templateUrl: './wildcard.component.html',
  styleUrls: ['./wildcard.component.scss']
})
export class WildcardComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private routingService: RoutingService,
    private router: Router
  ) { }

  ngOnInit() {
    const routePath: any = this.router.url.split('/');

    // This handles direct routing to the dynamic route when app hasn't built the router config
    // Subscribe from routing service a list of dynamic routes
    this.routingService.dynamicRoutes.subscribe((routes: any[]) => {
      const route: any = routes.filter(ele => ele.path === routePath[1])[0];
      if (route) {
        // pop off the empty slash
        routePath.shift();

        this.router.navigate(routePath);
      }
    });
  }
}
