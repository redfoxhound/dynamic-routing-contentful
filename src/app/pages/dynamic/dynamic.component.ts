import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentfulService } from 'src/app/services/contentful.service';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.scss']
})
export class DynamicComponent implements OnInit {

  contentId: string;
  contentData: object;

  constructor(
    private activatedRoute: ActivatedRoute,
    private contentfulService: ContentfulService,
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: any) => {

      console.log('DYNAMIC COMPONENT', data);

      if (data.id) {
        this.contentfulService.getEntry(data.id)
          .subscribe(res => {
            this.contentData = res;
          });
      }
    });
  }

}
