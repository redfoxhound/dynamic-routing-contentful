// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  contentful: {
    spaceID: '150hcx1xonjf',
    accessToken: 'e9136a34df717afeab63b1d43bea52f4c09f6e8b05e9bfdbb103ad15f16986b8',
    contentType: {
      lessons: 'lesson'
    },
    entry: {
      homepage: '2m26NMcF00a6aesc4KgSgS'
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
