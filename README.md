# DynamicRouting

This is a demo of dynamic routing. 

## Goal

To be able to set navigation dynamically from an external source (Contentful).

** It should be able to: **
- Build the menu and configure the router service during runtime
- Set custom component for each page
- Display the dynamic routes in the front-end
- Handle sub-pages
- Cache already fetched content
- Display content in dynamic component

## To-Do
- Clean Up code.
- Work on alternative model in Contentful

```js
{
  path: 'wireless',
  children: [
    {
      path: 'phones',
      component: DynamicComponent
    }
  ]
}
```